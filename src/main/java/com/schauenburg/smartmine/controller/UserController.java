package com.schauenburg.smartmine.controller;

import com.schauenburg.smartmine.model.user.User;
import com.schauenburg.smartmine.repository.UserRepository;
import com.schauenburg.smartmine.resource.UserResource;
import com.schauenburg.smartmine.resource.assembler.UserAssembler;
import com.schauenburg.smartmine.service.UserService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;

@RestController
@ExposesResourceFor(User.class)
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private UserAssembler assembler;

    @Autowired
    PasswordEncoder passwordEncoder;

    @PreAuthorize("#oauth2.hasScope('read') and hasAuthority('USER')")
    @RequestMapping(method = RequestMethod.GET)
    public Resources<UserResource> getUsers() {
        return new Resources<>(assembler.toResources(userRepo.findAll()));
    }

    @PreAuthorize("#oauth2.hasScope('read') and hasAuthority('USER')")
    @RequestMapping(method = RequestMethod.GET,params={"page","size","sort"})
    public PagedResources<UserResource> getPagedUsers(Pageable pageable, PagedResourcesAssembler assembler) {
        Page<User> userPage=  userRepo.findAll(pageable);
        return assembler.toResource(userPage,this.assembler);
    }


    @PreAuthorize("#oauth2.hasScope('read') and hasAuthority('USER')")
    @RequestMapping(value = "/role/{role}",method = RequestMethod.GET)
    public Resources<UserResource> getUsers(@PathVariable String role) {
        return new Resources<>(assembler.toResources(userRepo.findAllByRolesContains(role)));
    }

    @PreAuthorize("#oauth2.hasScope('read') and hasAuthority('USER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserResource> getUser(@PathVariable String id) {
        Optional<User> user = userRepo.findById(id);

        if (user.isPresent()) {
            return new ResponseEntity<>(assembler.toResource(user.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<UserResource> addUser(@RequestBody User user) throws Exception {
        String username = user.getUsername();
        String password = user.getPassword();

        if (    username == null ||
                username.trim().length() == 0 ||
                password == null ||
                password.trim().length() == 0){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        username = username.trim();
        password = password.trim();
        if (userService.newUser(username)) {
            return new ResponseEntity<>(assembler.toResource(userService.createUser(username, password)), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("#oauth2.hasScope('read') and hasAuthority('USER')")
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<UserResource> updateUser(@RequestBody User user) throws Exception {
        String username = user.getUsername();
        String password = user.getPassword();
        if (    username == null ||
                username.trim().length() == 0 ){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            username = username.trim();
            if (password != null) {
                password = password.trim();
                user.setPassword(passwordEncoder.encode(password));
            }
        }
        User existingUser = userRepo.findOneByUsername(username);
        if (existingUser == null) {
            return new ResponseEntity<>(assembler.toResource(userRepo.save(user)), HttpStatus.CREATED);
        } else if (!existingUser.getId().equalsIgnoreCase(user.getId())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("#oauth2.hasScope('read') and hasAuthority('USER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<UserResource> deletePerson(@PathVariable String id) {
        Optional<User> user = userRepo.findById(id);

        if (user.isPresent()) {
            user.get().setEnabled(false);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public ResponseEntity<?> hello(Principal principal) {
        Map<String, String> result = new HashMap<String, String>();
        result.put("message", "Registered phone number " + principal.getName());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


}
