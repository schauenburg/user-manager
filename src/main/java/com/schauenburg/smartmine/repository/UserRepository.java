package com.schauenburg.smartmine.repository;

import com.schauenburg.smartmine.model.user.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

/**
 * Access to the user data. JpaRepository grants us convenient access methods here.
 */
public interface UserRepository extends MongoRepository<User, String> {
    User findOneByUsername(String username);

    List<User> findAllByRolesContains(String role);
}
