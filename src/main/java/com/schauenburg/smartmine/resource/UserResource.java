package com.schauenburg.smartmine.resource;

import com.schauenburg.smartmine.model.user.User;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.core.Relation;

@Relation(value = "user", collectionRelation = "users")
public class UserResource extends Resource<User> {

    public UserResource(User content, Link... links) {
        super(content, links);
    }

}
