package com.schauenburg.smartmine.resource.assembler;

import com.schauenburg.smartmine.controller.UserController;
import com.schauenburg.smartmine.model.user.User;
import com.schauenburg.smartmine.resource.UserResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserAssembler extends ResourceAssemblerSupport<User, UserResource> {

    @Autowired
    EntityLinks links;

    public UserAssembler() {
        super(UserController.class, UserResource.class);
    }

    @Override
    public UserResource toResource(User user) {
        return new UserResource(user,
                links.linkToSingleResource(User.class, user.getId())) ;
    }

    @Override
    public List<UserResource> toResources(Iterable<? extends User> users) {
        return super.toResources(users);
    }
}

