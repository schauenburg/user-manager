package com.schauenburg.smartmine.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.hateoas.Identifiable;

import java.io.Serializable;
import java.util.*;


public class User implements Serializable, Identifiable<String> {
    @Id
    private String id;
    private String username;
    private String fullname;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private boolean enabled = true;
    boolean accountnonexpired = true;
    boolean credentialsnonexpired = true;
    boolean accountnonlocked = true;

    private Set<Role> roles = new HashSet<>();


    @DBRef
    private Set<Device> devices = new HashSet<>();

    public User() {
    }

    public User(String username, String password) {
        this();
        this.username = username;
        this.password = password;
    }

    public User(int id, String username, String password, Set<Role> roles, Boolean enabled) {
        super();
        this.username = username;
        this.password = password;
        this.roles = roles;
        this.enabled = enabled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Device> getDevices() {
        return devices;
    }

    public void setDevices(Set<Device> devices) {
        this.devices = devices;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isAccountnonexpired() {
        return accountnonexpired;
    }

    public void setAccountnonexpired(boolean accountnonexpired) {
        this.accountnonexpired = accountnonexpired;
    }

    public boolean isCredentialsnonexpired() {
        return credentialsnonexpired;
    }

    public void setCredentialsnonexpired(boolean credentialsnonexpired) {
        this.credentialsnonexpired = credentialsnonexpired;
    }

    public boolean isAccountnonlocked() {
        return accountnonlocked;
    }

    public void setAccountnonlocked(boolean accountnonlocked) {
        this.accountnonlocked = accountnonlocked;
    }


    public Optional<Device> getDeviceByName(String name){
        Optional<String> deviceName = Optional.of(name);
        if (deviceName.isPresent()) {
            for (Device device : getDevices()) {
                if (device.getName().equalsIgnoreCase(name)) {
                    return Optional.of(device);
                }
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }
}