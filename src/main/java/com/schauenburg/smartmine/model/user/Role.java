package com.schauenburg.smartmine.model.user;

public enum Role {
    ADMIN, USER;
}
