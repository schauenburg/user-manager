package com.schauenburg.smartmine.model.user;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.Date;

public class Device {
    private String id;
    private String name;
    private String token;
    private boolean active = true;
    private Date login = new Date();
    //TODO: Other device specific fields


    private Device() {
        id = new ObjectId().toString();
    }

    public Device(String name, String token) {
        this();
        this.name = name;
        this.token = token;
    }

    public Device(String token) {
        this();
        this.name = name;
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getLogin() {
        return login;
    }

    public void setLogin(Date login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Device device = (Device) o;

        return name.equals(device.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
