package com.schauenburg.smartmine.config;

import com.schauenburg.smartmine.model.user.Device;
import com.schauenburg.smartmine.model.user.Role;
import com.schauenburg.smartmine.model.user.User;
import com.schauenburg.smartmine.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class UserDeviceAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication auth)
            throws AuthenticationException {
        String username = auth.getName();
        String password = auth.getCredentials().toString();
        Map<String, String> details = (Map) auth.getDetails();

        User user = userRepository.findOneByUsername(username); //our own User model class
        if (user != null && user.getPassword() != null && details != null) {
            if (passwordEncoder.matches(password, user.getPassword())) {
                Optional<String> deviceName = Optional.ofNullable(details.get("device"));

                if (deviceName.isPresent()) {
                    Optional<String> registrationtoken = Optional.ofNullable(details.get("registrationtoken"));
                    if (registrationtoken.isPresent()) {
                        Optional<Device> device = user.getDeviceByName(deviceName.get());
                        Device currentDevice;

                        if (device.isPresent()){
                            currentDevice = device.get();
                            user.getDevices().add(currentDevice);
                        } else {
                            currentDevice = new Device(deviceName.get(), registrationtoken.get());
                        }

                        currentDevice.setLogin(new Date());
                        userRepository.save(user);

                        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                        for (Role role : user.getRoles()) {
                            authorities.add(new SimpleGrantedAuthority(role.name()));
                        }

                        return new UsernamePasswordAuthenticationToken(username, password, authorities);
                    } else {
                        throw new BadCredentialsException("Registration token is missing");
                    }
                }  else {
                    throw new BadCredentialsException("Device name is missing");
                }
            }
        }
        throw new BadCredentialsException("The user and/or device could not be authenticated");
    }

    @Override
    public boolean supports(Class<?> auth) {
        return auth.equals(UsernamePasswordAuthenticationToken.class);
    }
}
