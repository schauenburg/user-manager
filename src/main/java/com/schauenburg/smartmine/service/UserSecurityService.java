package com.schauenburg.smartmine.service;

import com.schauenburg.smartmine.model.user.Role;
import com.schauenburg.smartmine.model.user.User;
import com.schauenburg.smartmine.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;


@Service("userDetailsService")
public class UserSecurityService implements UserDetailsService {

    @Autowired
    private UserRepository userRepo;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) {

        User user = userRepo.findOneByUsername(username);
        if (user != null) {
            String password = user.getPassword();

            //additional information on the security object
            boolean enabled = user.isEnabled();
            boolean accountNonExpired = user.isAccountnonexpired();
            boolean credentialsNonExpired = user.isCredentialsnonexpired();
            boolean accountNonLocked = user.isAccountnonlocked();

            //Let’s populate user roles
            Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

            for (Role role : user.getRoles()) {
                authorities.add(new SimpleGrantedAuthority(role.name()));
            }

            //Now let’s create Spring Security User object
            org.springframework.security.core.userdetails.User securityUser = new
                    org.springframework.security.core.userdetails.User(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);

            return securityUser;

        } else {
            throw new UsernameNotFoundException("User not found !!!");

        }
    }

}
