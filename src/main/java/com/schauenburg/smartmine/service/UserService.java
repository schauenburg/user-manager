package com.schauenburg.smartmine.service;


import com.schauenburg.smartmine.model.user.User;
import com.schauenburg.smartmine.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserService {
    @Autowired
    private UserRepository userRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

/*    @Autowired
    private TopicMessenger messenger;*/

    public boolean newUser(String username){
        return userRepo.findOneByUsername(username) == null;
    }

    public User createUser(String username, String password){
        User user = new User();
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));

        if (newUser(username)) {
            user = userRepo.save(user);
        }
        //messenger.send(Action.USER_REGISTER, user);

        return user;
    }

}
